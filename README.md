# Distributor Native App

Folders:
- app: app code
- library: AppAuth 3rd party library

## Continuous Delivery
Continuous Delivery is active for this repository.
When pushing on the:
- `master` branch:
    - - The app can be downloaded in `https://gitlab.com/order-tracking/distributor_app/-/jobs/artifacts/master/download?job=build`
- `develop` branch:
    - The app can be downloaded in `https://gitlab.com/order-tracking/distributor_app/-/jobs/artifacts/develop/download?job=build`

## Generate APK
- `gradle assemble`

## Requirements to develop
- Android Studio 3

## Stack
- Retrofit
- Hypertrack
- AppAuth
