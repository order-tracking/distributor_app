package info.otrack.distributorapp;

import android.support.v7.app.AppCompatDelegate;
import com.hypertrack.lib.HyperTrack;

public final class Application extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        HyperTrack.initialize(this, BuildConfig.HYPERTRACK_KEY);
    }
}
