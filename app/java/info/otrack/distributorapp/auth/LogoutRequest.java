package info.otrack.distributorapp.auth;

import android.net.Uri;
import android.support.annotation.VisibleForTesting;

public class LogoutRequest {

    @VisibleForTesting
    static final String PARAM_REDIRECT_URI = "redirect_uri";

    private Uri logoutEndpoint;
    private Uri redirectUri;
    private String idToken;

    public LogoutRequest(Uri logoutEndpoint, Uri redirectUri, String idToken) {
        this.logoutEndpoint = logoutEndpoint;
        this.redirectUri = redirectUri;
        this.idToken = idToken;
    }

    public Uri toUri() {
        Uri.Builder uriBuilder = logoutEndpoint.buildUpon()
                .appendQueryParameter(PARAM_REDIRECT_URI, redirectUri.toString())
                .appendQueryParameter("id_token_hint", idToken);
        return uriBuilder.build();
    }
}
