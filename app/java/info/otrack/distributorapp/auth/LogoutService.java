package info.otrack.distributorapp.auth;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.customtabs.CustomTabsIntent;
import android.text.TextUtils;

import net.openid.appauth.browser.CustomTabManager;
import net.openid.appauth.internal.Logger;

import info.otrack.distributorapp.R;
import info.otrack.distributorapp.ui.activities.base.AuthActivity;

import static net.openid.appauth.Preconditions.checkNotNull;

public class LogoutService {

    @VisibleForTesting
    Context mContext;
    private CustomTabManager customTabManager;

    @NonNull
    private final BrowserHandler mBrowserHandler;

    private boolean mDisposed = false;

    public LogoutService(@NonNull Context context) {
        this(context, new BrowserHandler(context));
    }

    @VisibleForTesting
    public LogoutService(@NonNull Context context,
                         @NonNull BrowserHandler browserHandler) {
        mContext = checkNotNull(context);
        mBrowserHandler = checkNotNull(browserHandler);
    }

    public LogoutService(Context context, CustomTabManager customTabManager) {
        this.mContext = context;
        this.customTabManager = customTabManager;
        this.mBrowserHandler = new BrowserHandler(context);
    }

    public CustomTabsIntent.Builder createCustomTabsIntentBuilder() {
        checkNotDisposed();
        return mBrowserHandler.createCustomTabsIntentBuilder();
    }

    public void performLogoutRequest(
            @NonNull LogoutRequest request,
            @NonNull PendingIntent resultHandlerIntent) {

        performLogoutRequest(request,
                resultHandlerIntent,
                customTabManager.createTabBuilder().build());
    }

    public void performLogoutRequest(
            @NonNull LogoutRequest request,
            @NonNull PendingIntent resultHandlerIntent,
            @NonNull CustomTabsIntent customTabsIntent) {
        checkNotDisposed();
        Uri requestUri = request.toUri();
        PendingLogoutIntentStore.getInstance().addPendingIntent(request, resultHandlerIntent);
        Intent intent = customTabsIntent.intent;

        intent.setData(requestUri);
        if (TextUtils.isEmpty(intent.getPackage())) {
            intent.setPackage(mBrowserHandler.getBrowserPackage());
        }

        Logger.debug("Using %s as browser for auth", intent.getPackage());
        intent.putExtra(CustomTabsIntent.EXTRA_TITLE_VISIBILITY_STATE, CustomTabsIntent.NO_TITLE);
        intent.putExtra(CustomTabsIntent.EXTRA_TOOLBAR_COLOR, getColorCompat(R.color.colorPrimary));
        intent.putExtra(CustomTabsIntent.EXTRA_SECONDARY_TOOLBAR_COLOR, getColorCompat(R.color.colorWhite));
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        mContext.startActivity(intent);
    }

    public void dispose() {
        if (mDisposed) {
            return;
        }
        mBrowserHandler.unbind();
        mDisposed = true;
    }

    private void checkNotDisposed() {
        if (mDisposed) {
            throw new IllegalStateException("Service has been disposed and rendered inoperable");
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @SuppressWarnings("deprecation")
    private int getColorCompat(@ColorRes int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return mContext.getColor(color);
        } else {
            return mContext.getResources().getColor(color);
        }
    }
}
