package info.otrack.distributorapp.auth;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.hypertrack.lib.HyperTrack;
import com.hypertrack.lib.callbacks.HyperTrackCallback;
import com.hypertrack.lib.models.ErrorResponse;
import com.hypertrack.lib.models.SuccessResponse;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.AuthorizationServiceConfiguration;
import net.openid.appauth.AuthorizationServiceDiscovery;

import info.otrack.distributorapp.R;
import info.otrack.distributorapp.model.CurrentState;
import info.otrack.distributorapp.ui.activities.LoginActivity;

public class DoLogout {

    /**
     * Shared logout code
     *
     * Stops the tracking if is tracking and initiates a request to the end session endpoint
     * also removes the local state and runs a callback when finished
     *
     * @param ctx
     * @param mStateManager
     * @param mAuthService
     * @param callback
     */
    public static void start(Context ctx, AuthStateManager mStateManager, AuthorizationService mAuthService, Runnable callback) {
        CurrentState.clear(ctx); // clear state

        if(HyperTrack.isTracking()) { // Stop tracking first
            HyperTrack.stopTracking(new HyperTrackCallback() {
                @Override
                public void onSuccess(@NonNull SuccessResponse successResponse) {
                    executeLogout(ctx, mStateManager, mAuthService, callback);
                }

                @Override
                public void onError(@NonNull ErrorResponse errorResponse) {
                    Toast.makeText(ctx, "Hypertrack failed to stop tracking, try again.", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            executeLogout(ctx, mStateManager, mAuthService, callback);
        }
    }

    public static void start(Context ctx, AuthStateManager mStateManager, AuthorizationService mAuthService) {
        start(ctx, mStateManager, mAuthService, null);
    }

    private static void executeLogout(Context ctx, AuthStateManager mStateManager, AuthorizationService mAuthService, Runnable callback) {
        mStateManager.getCurrent().performActionWithFreshTokens(mAuthService, (accessToken, idToken, ex) -> {
            AuthState currentState = mStateManager.getCurrent();
            AuthorizationServiceConfiguration sConfig = currentState.getAuthorizationServiceConfiguration();

            if (ex != null) {
                if(sConfig != null) {
                    AuthState clearedState =
                            new AuthState(sConfig);
                    if (currentState.getLastRegistrationResponse() != null) {
                        clearedState.update(currentState.getLastRegistrationResponse());
                    }
                    mStateManager.replace(clearedState);
                }

                ctx.startActivity(new Intent(ctx, LoginActivity.class));
                return;
            }

            try {
                AuthorizationServiceDiscovery discoveryDoc = sConfig.discoveryDoc;
                if (discoveryDoc == null) {
                    throw new IllegalStateException("no available discovery doc");
                }

                Uri endSessionEndpoint = Uri.parse(discoveryDoc.getEndSessionEndpoint().toString());

                String logoutUri = ctx.getResources().getString(R.string.auth_logout_uri);
                LogoutRequest logoutRequest = new LogoutRequest(endSessionEndpoint,
                        Uri.parse(logoutUri), idToken);

                LogoutService logoutService = new LogoutService(ctx, mAuthService.getCustomTabManager());
                logoutService.performLogoutRequest(
                        logoutRequest,
                        PendingIntent.getActivity(
                                ctx, logoutRequest.hashCode(),
                                new Intent(ctx, LoginActivity.class), 0)
                );

                if(callback != null) {
                    callback.run();
                }
            } catch (Exception e) {
                Toast.makeText(ctx, "Failed to logout: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }

        });
    }
}