package info.otrack.distributorapp.data.service;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import info.otrack.distributorapp.data.api.IOTrackServiceAPI;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    public static <S> S createService(Class<S> serviceClass) {
        return createService(serviceClass, null);
    }

    public static <S> S createService(
            Class<S> serviceClass, final String authToken) {
        GsonBuilder gson = new GsonBuilder();

        // Serialize dates
        gson.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSS");

            @Override
            public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
                    throws JsonParseException {
                try {
                    String date = json.getAsString();

                    // Convert to local timezone from UTC
                    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
                    TimeZone tz = cal.getTimeZone();

                    df.setTimeZone(tz);
                    Date parsed = df.parse(date);

                    return parsed;
                } catch (ParseException e) {
                    return null;
                }
            }
        });

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(IOTrackServiceAPI.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson.create()));

        if (!TextUtils.isEmpty(authToken)) {
            Interceptor interceptor = chain -> {
                Request original = chain.request();

                Request.Builder builder1 = original.newBuilder()
                        .header("Authorization", "Bearer " + authToken);

                Request request = builder1.build();
                return chain.proceed(request);
            };


            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor);

                builder.client(httpClient.build());
            }
        }

        return builder.build().create(serviceClass);
    }

}
