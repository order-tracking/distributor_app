package info.otrack.distributorapp.data.repository;

import java.io.IOException;
import java.util.List;

import info.otrack.distributorapp.data.api.IOTrackServiceAPI;
import info.otrack.distributorapp.data.repository.base.IoTrackRepository;
import info.otrack.distributorapp.model.Journey;
import info.otrack.distributorapp.model.Order;
import retrofit2.Call;
import retrofit2.Callback;

public class oTrackRepository implements IoTrackRepository {

    private final IOTrackServiceAPI api;

    public oTrackRepository(IOTrackServiceAPI api) {
        this.api = api;
    }

    @Override
    public Journey getJourneySync(int id) throws IOException {
        Call<Journey> journey = api.getJourney(id);
        return journey.execute().body();
    }

    @Override
    public Order getOrderSync(int id) throws IOException {
        Call<Order> order = api.getOrder(id);
        return order.execute().body();
    }

    @Override
    public Journey editJourneySync(Journey journey) throws IOException {
        Call<Journey> editedJourney = api.editJourney(journey.id, journey);
        return editedJourney.execute().body();
    }

    @Override
    public List<Journey> getEmployeeIncompleteJourneys(int employeeId) throws IOException {
        Call<List<Journey>> journeys = api.getEmployeeIncompleteJourneys(employeeId);
        return journeys.execute().body();
    }

    @Override
    public void getJourney(int id, Callback<Journey> callback) throws IOException {
        Call<Journey> journey = api.getJourney(id);
        journey.enqueue(callback);
    }

    @Override
    public void editJourney(Journey journey, Callback<Journey> callback) throws IOException {
        Call<Journey> editedJourney = api.editJourney(journey.id, journey);
        editedJourney.enqueue(callback);
    }
}
