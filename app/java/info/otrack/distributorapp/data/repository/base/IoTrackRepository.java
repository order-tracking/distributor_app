package info.otrack.distributorapp.data.repository.base;

import java.io.IOException;
import java.util.List;

import info.otrack.distributorapp.model.Journey;
import info.otrack.distributorapp.model.Order;
import retrofit2.Callback;

public interface IoTrackRepository {
    Journey getJourneySync(int id) throws IOException;

    Order getOrderSync(int id) throws IOException;

    Journey editJourneySync(Journey journey) throws IOException;

    List<Journey> getEmployeeIncompleteJourneys(int employeeId) throws IOException;

    void getJourney(int id, Callback<Journey> callback) throws IOException;

    void editJourney(Journey journey, Callback<Journey> callback) throws IOException;
}
