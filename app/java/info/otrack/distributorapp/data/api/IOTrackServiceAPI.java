package info.otrack.distributorapp.data.api;

import java.util.List;

import info.otrack.distributorapp.BuildConfig;
import info.otrack.distributorapp.model.Journey;
import info.otrack.distributorapp.model.Order;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IOTrackServiceAPI {
    String BASE_URL = BuildConfig.OTRACK_API;

    /**
     * Retrieves a single journey from the OTrack API
     * @param id - Unique Identifier of the Journey
     * @return Journey entity
     */
    @GET("/journeys/{id}")
    Call<Journey> getJourney(@Path("id") int id);

    @GET("/orders/{id}")
    Call<Order> getOrder(@Path("id") int id);

    /**
     * Retrieves listed incomplete journeys related to an employee from the OTrack API.
     * @param employeeId - Unique identifier of the employee
     * @return List of incomplete journeys
     */
    @GET("/journeys/{employeeId}/incomplete")
    Call<List<Journey>> getEmployeeIncompleteJourneys(@Path("employeeId") int employeeId);

    /**
     * Updates a single Journey to the OTrack API.
     * @param id - Unique identifier of the Journey
     * @param journey - Journey entity
     * @return Updated Journey entity
     */
    @PUT("/journeys/{id}")
    Call<Journey> editJourney(@Path("id") int id, @Body Journey journey);

    /**
     * Updates a single Order entity to the OTrack API.
     * @param id - Unique identifier of the Order
     * @param order - Order entity
     * @return Updated Order entity
     */
    @PUT("/orders/{id}")
    Call<Order> editOrder(@Path("id") int id, @Body Order order);
}
