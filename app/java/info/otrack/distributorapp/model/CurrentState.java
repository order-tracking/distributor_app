package info.otrack.distributorapp.model;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class CurrentState {

    private static String PREFS_NAME = "deliveryInfo";
    private static String KEY_ORDER_ID = "orderId";
    private static String KEY_ORDERS_COMPLETED = "ordersCompleted";

    /**
     * Get the order id for the current delivery or throws if not making a delivery
     * @param ctx
     * @return
     * @throws Exception
     */
    public static int getDelivery(Context ctx) throws Exception {
        SharedPreferences prefs = ctx.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        int val = prefs.getInt(KEY_ORDER_ID, -1);

        if(val == -1) {
            throw new Exception("No current delivery at the moment");
        }

        return val;
    }

    /**
     * Sets the current delivery as the order id
     *
     * @param ctx
     * @param id
     */
    public static void setDelivery(Context ctx, int id) {
        SharedPreferences.Editor editor = ctx.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();

        editor.putInt(KEY_ORDER_ID, id);
        editor.apply();
    }

    /**
     * Clear the actual delivery
     *
     * @param ctx
     */
    public static void clearDelivery(Context ctx) {
        SharedPreferences.Editor editor = ctx.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();

        editor.remove(KEY_ORDER_ID);
        editor.apply();
    }

    public static void markAsCompleted(Context ctx, int id) {
        SharedPreferences prefs = ctx.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = ctx.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();

        String ids = prefs.getString(KEY_ORDERS_COMPLETED, "");

        editor.putString(KEY_ORDERS_COMPLETED, ids + "," + id);
        editor.apply();
    }

    public static boolean isCompleted(Context ctx, int id) {
        SharedPreferences prefs = ctx.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        String[] ids = prefs.getString(KEY_ORDERS_COMPLETED, "").split(",");

        for(int i = 0; i < ids.length; ++i) {
            if(ids[i].equals(id+"")) {
                return true;
            }
        }

        return false;
    }

    public static void clear(Context ctx) {
        ctx.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit().clear().apply();
    }



}
