package info.otrack.distributorapp.model;

import java.util.Date;
import java.util.List;

public class Journey {
    public int id;

    public Boolean completed;

    public Employee employee;

    public Date createdAt;

    public Date finishedAt = null;

    public List<Order> orders;
}
