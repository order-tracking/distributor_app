package info.otrack.distributorapp.model;

public class Item {
    public int id;
    public String note;
    public int quantity;
    public Boolean completed;
    public int orderId;
    public int processId;
    public Product product;
    public ProductType productType;
}

