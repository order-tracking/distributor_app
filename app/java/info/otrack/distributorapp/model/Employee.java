package info.otrack.distributorapp.model;

public class Employee {
    public int id;

    public int companyId;

    public String companyName;

    public String name;

    public String phoneNumber;

    public String role;

}
