package info.otrack.distributorapp.model;

import java.util.List;

/**
 * Customer Dto
 */
public class Customer {
    public int id;

    public String name;

    public String phoneNumber;

    public List<Order> Orders;
}
