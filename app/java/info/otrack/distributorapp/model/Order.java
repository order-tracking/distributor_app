package info.otrack.distributorapp.model;

import java.util.Date;
import java.util.List;

public class Order {
    public int id;
    public String address;
    public String address2;
    public String postalCode;

    public double latitude;
    public double longitude;

    public int distance;
    public int estimatedTime;
    public Boolean readyToDistribute;
    public Boolean onGoing;
    public Date createdAt;
    public Date deliveredAt;
    public Customer customer;
    public List<Item> items;

}
