package info.otrack.distributorapp.ui.presenters;

import android.os.AsyncTask;
import android.support.annotation.MainThread;
import android.util.Log;

import net.openid.appauth.AuthorizationException;

import java.util.List;

import info.otrack.distributorapp.contracts.ILoadDataView;
import info.otrack.distributorapp.data.api.IOTrackServiceAPI;
import info.otrack.distributorapp.data.repository.oTrackRepository;
import info.otrack.distributorapp.data.repository.base.IoTrackRepository;
import info.otrack.distributorapp.data.service.ServiceGenerator;
import info.otrack.distributorapp.helpers.Utils;
import info.otrack.distributorapp.model.Employee;
import info.otrack.distributorapp.model.Journey;
import info.otrack.distributorapp.ui.presenters.base.ListablePresenter;

public class JourneyListPresenter extends ListablePresenter<List<Journey>> {

    public JourneyListPresenter(
            ILoadDataView<List<Journey>> view) {
        super(view);
    }

    @Override
    public void getMoreData(int page) {
    }


    @Override
    public void refresh() {
        view.showError("Refreshing");
        this.execute();
    }

    @MainThread
    @Override
    public void execute() {
        mStateManager.getCurrent().performActionWithFreshTokens(mAuthService, this::fetchList);
    }

    @MainThread
    private void fetchList(String accessToken, String refreshToken, AuthorizationException ex) {
        if(ex != null) {
            String msg = ex.getCause().getMessage();
            view.resetAuth(msg);
            return;
        }

        new LoadDataTask(accessToken).execute();
    }

    /**
     * Load journeys in a worker thread using an AsyncTask
     */
    private class LoadDataTask extends AsyncTask<Void, Void, List<Journey>> {
        private String accessToken;

        LoadDataTask(String accessToken) {
            this.accessToken = accessToken;
        }

        @Override
        protected List<Journey> doInBackground(Void... params) {
            Log.d(TAG, "doInBackground: Getting journeys from api");

            IOTrackServiceAPI api = ServiceGenerator.createService(IOTrackServiceAPI.class, this.accessToken);
            IoTrackRepository repo = new oTrackRepository(api);

            Employee user = Utils.getEmployeeFromAccessToken(accessToken);

            try {
                return repo.getEmployeeIncompleteJourneys(user.id);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Journey> list) {
            super.onPostExecute(list);

            if(view == null) return;

            if(list == null) {

                if(!Utils.isNetworkConnected(view.getViewContext())) {
                    view.showNoConnection();
                } else {
                    view.showError("Failed to load the list.");
                }

                return;
            }

            view.setData(list);
        }
    }
}
