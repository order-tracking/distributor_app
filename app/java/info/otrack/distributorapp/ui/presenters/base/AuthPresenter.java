package info.otrack.distributorapp.ui.presenters.base;

import net.openid.appauth.AppAuthConfiguration;
import net.openid.appauth.AuthorizationService;

import info.otrack.distributorapp.auth.AuthStateManager;
import info.otrack.distributorapp.auth.Configuration;
import info.otrack.distributorapp.contracts.ILoadDataView;

public abstract class AuthPresenter<T> extends Presenter<T> {

    public AuthorizationService mAuthService;
    public AuthStateManager mStateManager;

    public AuthPresenter(ILoadDataView<T> view) {
        super(view);

        mStateManager = AuthStateManager.getInstance(view.getViewContext());
        Configuration config = Configuration.getInstance(view.getViewContext());

        mAuthService = new AuthorizationService(
                view.getViewContext(), new AppAuthConfiguration.Builder()
                .setConnectionBuilder(config.getConnectionBuilder())
                .build());
    }

    @Override
    public void onDestroy() {
        mAuthService.dispose();

        mStateManager = null;
        mAuthService = null;

       super.onDestroy();
    }
}
