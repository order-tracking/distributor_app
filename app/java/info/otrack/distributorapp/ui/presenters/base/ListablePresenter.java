package info.otrack.distributorapp.ui.presenters.base;

import info.otrack.distributorapp.contracts.ILoadDataView;

public abstract class ListablePresenter<T> extends AuthPresenter<T> {

    public ListablePresenter(ILoadDataView<T> view) {
        super(view);
    }

    public abstract void getMoreData(int page);

    public abstract void refresh();
}
