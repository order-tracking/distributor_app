package info.otrack.distributorapp.ui.presenters;

import android.os.AsyncTask;
import android.util.Log;

import net.openid.appauth.AuthorizationException;

import info.otrack.distributorapp.contracts.ILoadDataView;
import info.otrack.distributorapp.data.api.IOTrackServiceAPI;
import info.otrack.distributorapp.data.repository.oTrackRepository;
import info.otrack.distributorapp.data.repository.base.IoTrackRepository;
import info.otrack.distributorapp.data.service.ServiceGenerator;
import info.otrack.distributorapp.helpers.Utils;
import info.otrack.distributorapp.model.Journey;
import info.otrack.distributorapp.ui.presenters.base.AuthPresenter;

public class JourneyDetailsPresenter extends AuthPresenter<Journey> {
    private int id;

    public JourneyDetailsPresenter(ILoadDataView<Journey> view) {
        super(view);
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public void execute() {
        mStateManager.getCurrent().performActionWithFreshTokens(mAuthService, (String accessToken,
                                                                               String refreshToken,
                                                                               AuthorizationException ex) -> {
            if (ex != null) {
                view.resetAuth(ex.getCause().getMessage());
                return;
            }

            new JourneyDetailsPresenter.LoadDataTask(accessToken).execute(id);
        });
    }

    private class LoadDataTask extends AsyncTask<Integer, Void, Journey> {
        private String accessToken;

        LoadDataTask(String accessToken) {
            this.accessToken = accessToken;
        }

        @Override
        protected Journey doInBackground(Integer... params) {
            Log.d(TAG, "doInBackground: Getting journeys from api");

            IOTrackServiceAPI api = ServiceGenerator.createService(IOTrackServiceAPI.class, this.accessToken);
            IoTrackRepository repo = new oTrackRepository(api);

            try {
                int id = params[0];
                return repo.getJourneySync(id);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Journey data) {
            super.onPostExecute(data);

            if(view == null) return;

            if (data == null) {
                if(!Utils.isNetworkConnected(view.getViewContext())) {
                    view.showNoConnection();
                } else {
                    view.showError("Failed to load the Journey.");
                }
                return;
            }

            view.setData(data);
        }
    }


}
