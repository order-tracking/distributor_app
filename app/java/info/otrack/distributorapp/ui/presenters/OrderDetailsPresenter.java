package info.otrack.distributorapp.ui.presenters;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.hypertrack.lib.HyperTrack;
import com.hypertrack.lib.callbacks.HyperTrackCallback;
import com.hypertrack.lib.models.Action;
import com.hypertrack.lib.models.ActionParams;
import com.hypertrack.lib.models.ActionParamsBuilder;
import com.hypertrack.lib.models.ErrorResponse;
import com.hypertrack.lib.models.Place;
import com.hypertrack.lib.models.SuccessResponse;
import com.hypertrack.lib.models.User;
import com.hypertrack.lib.models.UserParams;

import net.openid.appauth.AuthorizationException;

import java.util.ArrayList;
import java.util.List;

import info.otrack.distributorapp.contracts.ILoadDataView;
import info.otrack.distributorapp.data.api.IOTrackServiceAPI;
import info.otrack.distributorapp.data.repository.oTrackRepository;
import info.otrack.distributorapp.data.repository.base.IoTrackRepository;
import info.otrack.distributorapp.data.service.ServiceGenerator;
import info.otrack.distributorapp.helpers.Utils;
import info.otrack.distributorapp.model.CurrentState;
import info.otrack.distributorapp.model.Employee;
import info.otrack.distributorapp.model.Order;
import info.otrack.distributorapp.ui.activities.base.AuthActivity;
import info.otrack.distributorapp.ui.fragments.OrderDetailsFragment;
import info.otrack.distributorapp.ui.presenters.base.AuthPresenter;

import static com.hypertrack.lib.models.Action.ACTION_TYPE_DELIVERY;

/**
 * Manages the UI of the OrderDetailsFragment
 *
 * Fetches data a updates the view
 */
public class OrderDetailsPresenter extends AuthPresenter<Order> {
    private int id;
    private Employee user;
    private OrderDetailsFragment fragment;

    public OrderDetailsPresenter(ILoadDataView<Order> view) {
        super(view);

        // we need some specific UI methods from the fragment
        fragment = (OrderDetailsFragment) view;

        // we know the view somehow inherits from AuthActivity
        user = ((AuthActivity) view.getViewContext()).user;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Load Order details from API
     */
    @Override
    public void execute() {
        mStateManager.getCurrent().performActionWithFreshTokens(mAuthService, (String accessToken,
                                                                               String refreshToken,
                                                                               AuthorizationException ex) -> {
            if (ex != null) {
                view.resetAuth(ex.getCause().getMessage());
                return;
            }

            new OrderDetailsPresenter.LoadDataTask(accessToken).execute(id);
        });
    }

    private class LoadDataTask extends AsyncTask<Integer, Void, Order> {
        private String accessToken;

        LoadDataTask(String accessToken) {
            this.accessToken = accessToken;
        }

        @Override
        protected Order doInBackground(Integer... params) {
            Log.d(TAG, "doInBackground: Getting journeys from api");

            IOTrackServiceAPI api = ServiceGenerator.createService(IOTrackServiceAPI.class, this.accessToken);
            IoTrackRepository repo = new oTrackRepository(api);

            try {
                int id = params[0];
                return repo.getOrderSync(id);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Order data) {
            super.onPostExecute(data);

            if(view == null) return;

            if (data == null) {
                if(!Utils.isNetworkConnected(view.getViewContext())) {
                    view.showNoConnection();
                } else {
                    view.showError("Failed to load the Order.");
                }
                return;
            }

            view.setData(data);
        }
    }

    /**
     * Create the Hypertrack action and assign to the current employee
     *
     * @param order
     */
    public void toggleDelivery(Order order) {
        try {
            // already in progress, we need to mark as done
            int id = CurrentState.getDelivery(view.getViewContext());
            stopDelivery(order);
        } catch (Exception e) {
            // no current delivery, start a new one
            fragment.beginDelivery();

            createAction(order);
        }
    }

    private void createAction(Order order) {
        Place expectedPlace;

        if(order.latitude == 0 && order.longitude == 0) {
            // coordinates do not exist, fallback to the address
            expectedPlace = new Place().setAddress(order.address).setZipCode(order.postalCode);
        } else {
            expectedPlace = new Place().setLocation(order.latitude, order.longitude);
        }

        ActionParams actionParams = new ActionParamsBuilder().setExpectedPlace(expectedPlace)
                .setType(ACTION_TYPE_DELIVERY)
                .setLookupId(order.id + "").build();



        HyperTrack.createAndAssignAction(actionParams, new HyperTrackCallback() {
            @Override
            public void onSuccess(@NonNull SuccessResponse response) {
                Action action = (Action) response.getResponseObject();

                CurrentState.setDelivery(view.getViewContext(), order.id);
                fragment.deliveryStarted();
                Toast.makeText(view.getViewContext(), "Delivery Started!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(@NonNull ErrorResponse errorResponse) {
                view.showError("Failed to create the Action: " + errorResponse.getErrorMessage());
                fragment.deliveryFailed();
            }
        });
    }


    /**
     * Mark the action as completed
     * @param order
     */
    public void stopDelivery(Order order) {
        fragment.beginEndDelivery();

        HyperTrack.completeActionWithLookupIdInSync(order.id + "", new HyperTrackCallback() {

            @Override
            public void onSuccess(@NonNull SuccessResponse successResponse) {
                CurrentState.clearDelivery(view.getViewContext());
                CurrentState.markAsCompleted(view.getViewContext(), order.id);
                fragment.deliveryEnded();

                Toast.makeText(view.getViewContext(), "Delivery completed!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(@NonNull ErrorResponse errorResponse) {
                view.showError("Failed to complete the action, try again");
                CurrentState.clearDelivery(view.getViewContext());
            }
        });

    }

}
