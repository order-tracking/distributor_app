package info.otrack.distributorapp.ui.activities;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;

import net.openid.appauth.AppAuthConfiguration;
import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.internal.Logger;


import info.otrack.distributorapp.auth.AuthStateManager;
import info.otrack.distributorapp.auth.Configuration;
import info.otrack.distributorapp.auth.PendingLogoutIntentStore;

public class LogoutUriReceiverActivity extends Activity {

    private static final String KEY_STATE = "state";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PendingIntent target = PendingLogoutIntentStore.getInstance().getPendingIntent();

        AuthStateManager mStateManager = AuthStateManager.getInstance(this);

        AuthState currentState = mStateManager.getCurrent();
        AuthState clearedState =
                new AuthState(currentState.getAuthorizationServiceConfiguration());
        if (currentState.getLastRegistrationResponse() != null) {
            clearedState.update(currentState.getLastRegistrationResponse());
        }

        mStateManager.replace(clearedState);

        Logger.debug("Forwarding redirect");
        try {
            target.send(this, 0, null);
        } catch (PendingIntent.CanceledException e) {
            Logger.errorWithStack(e, "Unable to send pending intent");
        }

        finish();
    }
}