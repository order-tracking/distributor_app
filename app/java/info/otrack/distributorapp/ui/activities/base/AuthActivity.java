package info.otrack.distributorapp.ui.activities.base;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import net.openid.appauth.AppAuthConfiguration;
import net.openid.appauth.AuthorizationService;

import info.otrack.distributorapp.R;
import info.otrack.distributorapp.auth.AuthStateManager;
import info.otrack.distributorapp.auth.Configuration;
import info.otrack.distributorapp.auth.DoLogout;
import info.otrack.distributorapp.helpers.Utils;
import info.otrack.distributorapp.model.Employee;

/**
 * Activity with user info
 */
public abstract class AuthActivity extends ToolbarActivity {

    public AuthorizationService mAuthService;
    public AuthStateManager mStateManager;
    public Employee user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mStateManager = AuthStateManager.getInstance(this);
        Configuration config = Configuration.getInstance(this);

        mAuthService = new AuthorizationService(
                this, new AppAuthConfiguration.Builder()
                .setConnectionBuilder(config.getConnectionBuilder())
                .build());

        user = Utils.getEmployeeFromAccessToken(mStateManager.getCurrent().getAccessToken());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_auth, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_signout:
                logout();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAuthService.dispose();
    }

    public void logout() {
        DoLogout.start(this, mStateManager, mAuthService);
    }
}
