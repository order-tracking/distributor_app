package info.otrack.distributorapp.ui.activities;

import android.os.Bundle;
import info.otrack.distributorapp.R;


import info.otrack.distributorapp.ui.activities.base.AuthActivity;
import info.otrack.distributorapp.ui.fragments.JourneysFragment;


public class JourneyActivity  extends AuthActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setTitle("Pending Journeys");

        JourneysFragment firstFragment = new JourneysFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.content, firstFragment).commit();

        this.enableBackButton();
    }

}
