package info.otrack.distributorapp.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.widget.Toast;

import info.otrack.distributorapp.R;
import info.otrack.distributorapp.helpers.Utils;
import info.otrack.distributorapp.model.Journey;
import info.otrack.distributorapp.ui.activities.base.AuthActivity;
import info.otrack.distributorapp.ui.fragments.OrderDetailsFragment;

/**
 * Activity to display the journey details
 */
public class OrderDetailsActivity extends AuthActivity {

    public static final String ID_TAG = "order_id";
    private static final String JOURNEY_ID_TAG = "journey_id";
    private static final String JOURNEY_DATE_TAG = "journey_date";

    private int id;
    private int journeyId;
    private String journeyDate;
    private OrderDetailsFragment firstFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        id = getIntent().getIntExtra(ID_TAG, 0);
        journeyId = getIntent().getIntExtra(JOURNEY_ID_TAG, 0);
        journeyDate = getIntent().getStringExtra(JOURNEY_DATE_TAG);

        getSupportActionBar().setTitle("Order #" + id);
        this.enableBackButton();

        // create and add the fragment
        firstFragment = new OrderDetailsFragment();
        Bundle b = new Bundle();
        b.putInt(ID_TAG, id);
        firstFragment.setArguments(b);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.content, firstFragment).commit();
    }

    /**
     * Create the intent to navigate to this activity
     * @param context
     * @param id
     * @return
     */
    public static Intent createIntent(Context context, int id, Journey jouney) {
        Intent intent = new Intent(context, OrderDetailsActivity.class);
        intent.putExtra(ID_TAG, id);
        intent.putExtra(JOURNEY_ID_TAG, jouney.id);
        intent.putExtra(JOURNEY_DATE_TAG, Utils.getHoursFromDate(jouney.createdAt));

        return intent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Back Button
            case android.R.id.home:
                Intent up = NavUtils.getParentActivityIntent(this);
                up.putExtra(JourneyDetailsActivity.ID_TAG, journeyId);
                up.putExtra(JourneyDetailsActivity.DATE_TAG, journeyDate);
                NavUtils.navigateUpTo(this, up);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Handle on call permission result
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions,
                grantResults);

        if(requestCode == OrderDetailsFragment.REQUEST_CALL) {
            if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                firstFragment.onCall();
            } else {
                Toast.makeText(this, "Call Permission denied.",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }


}