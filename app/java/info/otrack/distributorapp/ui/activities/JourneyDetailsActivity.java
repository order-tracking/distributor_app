package info.otrack.distributorapp.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import info.otrack.distributorapp.R;

import info.otrack.distributorapp.ui.activities.base.AuthActivity;
import info.otrack.distributorapp.ui.fragments.JourneyDetailsFragment;

/**
 * Activity to display the journey details
 */
public class JourneyDetailsActivity extends AuthActivity {


    public static final String ID_TAG = "journey_id";
    public static final String DATE_TAG = "journey_date";

    private int journeyId;
    private String journeyDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        journeyId = getIntent().getIntExtra(ID_TAG, 0);
        journeyDate = getIntent().getStringExtra(DATE_TAG);

        getSupportActionBar().setTitle("Journey #" + journeyId + " " + journeyDate);
        this.enableBackButton();

        // create and add the fragment
        JourneyDetailsFragment firstFragment = new JourneyDetailsFragment();
        Bundle b = new Bundle();
        b.putInt(ID_TAG, journeyId);
        firstFragment.setArguments(b);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.content, firstFragment).commit();
    }

    /**
     * Create the intent to navigate to this activity
     * @param context
     * @param id
     * @return
     */
    public static Intent createIntent(Context context, int id, String date) {
        Intent intent = new Intent(context, JourneyDetailsActivity.class);
        intent.putExtra(ID_TAG, id);
        intent.putExtra(DATE_TAG, date);

        return intent;
    }
}