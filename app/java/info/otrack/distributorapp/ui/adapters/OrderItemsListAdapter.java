package info.otrack.distributorapp.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import info.otrack.distributorapp.R;

import java.util.List;

import info.otrack.distributorapp.model.Item;
import info.otrack.distributorapp.model.Order;

public class OrderItemsListAdapter extends ArrayAdapter<Item> {

    // View lookup cache
    private static class ViewHolder {
        TextView name;
        TextView type;
    }

    public OrderItemsListAdapter(Context context, List<Item> items) {
        super(context, R.layout.list_order_item_row_layout, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Item item = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            // If there's no view to re-use, inflate a brand new view for row
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_order_item_row_layout, parent, false);


            viewHolder.name = convertView.findViewById(R.id.item_name);
            viewHolder.type = convertView.findViewById(R.id.item_type);

            convertView.setTag(viewHolder);
        } else {
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Populate the data from the data object via the viewHolder object
        // into the template view.
        viewHolder.name.setText(item.product.name);

        String qty = "Qtd: " + item.quantity;
        if(item.productType != null) qty += " " + item.productType.name;

        viewHolder.type.setText(qty);

        // Return the completed view to render on screen
        return convertView;
    }
}