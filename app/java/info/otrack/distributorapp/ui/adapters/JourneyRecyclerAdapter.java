package info.otrack.distributorapp.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import info.otrack.distributorapp.R;

import java.util.List;

import info.otrack.distributorapp.helpers.Utils;
import info.otrack.distributorapp.model.Journey;

/**
 * Adapter used to show a list of journeys
 */
public class JourneyRecyclerAdapter extends RecyclerView.Adapter<JourneyRecyclerAdapter.ViewHolder> {

    private List<Journey> data;
    private Context context;
    private IClickListener         listener;

    /**
     * Constructs a new JourneyRecyclerAdapter with a context
     *
     * @param ctx
     */
    public JourneyRecyclerAdapter(Context ctx) {
        this.context = ctx;
    }

    /**
     * Replace data in the adapter
     *
     * @param data
     */
    public void setData(List<Journey> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    /**
     * Sets the click listener
     * @param listener
     */
    public void setListener(IClickListener listener) {
        this.listener = listener;
    }

    /**
     * Clear all data from adapter
     */
    public void clearData() {
        data.clear();
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_journeys_row_layout, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Journey journey = data.get(position);

        String date = Utils.getHoursFromDate(journey.createdAt);

        holder.title.setText("Journey #" + journey.id + " " + date);

        int total = journey.orders.size();
        String orders = "order";
        if(total > 1) orders = "orders";
        holder.desc.setText(total + " " + orders);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    /**
     * Interface for click listeners for this adapter
     */
    public interface IClickListener {
        void onItemClick(Journey journey);
    }

    /**
     * List row members
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title;
        public TextView  desc;

        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.journey_title_details);
            desc = itemView.findViewById(R.id.title_original);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(data.get(getAdapterPosition()));
        }
    }
}
