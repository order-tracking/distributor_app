package info.otrack.distributorapp.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import info.otrack.distributorapp.R;

import java.text.DecimalFormat;
import java.util.List;

import info.otrack.distributorapp.helpers.Utils;
import info.otrack.distributorapp.model.CurrentState;
import info.otrack.distributorapp.model.Order;

public class OrderListAdapter extends ArrayAdapter<Order> {
    // View lookup cache
    private static class ViewHolder {
        TextView address;
        TextView desc;
        public ImageView img;
    }

    public OrderListAdapter(Context context, List<Order> orders) {
        super(context, R.layout.list_order_row_layout, orders);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Order order = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            // If there's no view to re-use, inflate a brand new view for row
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_order_row_layout, parent, false);


            viewHolder.img = convertView.findViewById(R.id.imageView3);
            viewHolder.address = convertView.findViewById(R.id.address);
            viewHolder.desc = convertView.findViewById(R.id.order_desc);

            convertView.setTag(viewHolder);
        } else {
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.img.setBackgroundResource(R.drawable.ic_shopping_cart_primary_24dp);

        int limit = order.address.length() < 30 ? order.address.length() : 30;

        viewHolder.address.setText(order.address.substring(0, limit)); // limit chars
        viewHolder.address.setTextColor(parent.getContext().getResources().getColor(R.color.primary_text_default_material_light));

        String status = "Pending delivery";

        if(order.onGoing) {
            status = "Current Delivery";
            viewHolder.address.setTextColor(parent.getContext().getResources().getColor(R.color.colorError));
        } else {
            try {
                int id = CurrentState.getDelivery(parent.getContext());
                if(order.id == id) {
                    status = "Current Delivery";
                    viewHolder.address.setTextColor(parent.getContext().getResources().getColor(R.color.colorError));
                }
            } catch (Exception e) {
            }
        }

        if (order.deliveredAt != null || CurrentState.isCompleted(parent.getContext(), order.id)) {
            status = "Delivered";
            viewHolder.address.setTextColor(parent.getContext().getResources().getColor(R.color.colorSuccess));
            viewHolder.img.setBackgroundResource(R.drawable.ic_check_primary_24dp);
        }

        String km = "? km";
        if (order.distance > 0) {
            DecimalFormat df = new DecimalFormat("#.#");
            km = df.format(((double) order.distance) / 1000) + " km";
        }

        viewHolder.desc.setText(km + " - " + status);

        // Return the completed view to render on screen
        return convertView;
    }
}