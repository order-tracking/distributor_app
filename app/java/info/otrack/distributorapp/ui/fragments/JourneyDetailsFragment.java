package info.otrack.distributorapp.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import info.otrack.distributorapp.R;


import info.otrack.distributorapp.helpers.Utils;
import info.otrack.distributorapp.model.Journey;
import info.otrack.distributorapp.model.Order;
import info.otrack.distributorapp.ui.activities.JourneyDetailsActivity;
import info.otrack.distributorapp.ui.activities.OrderDetailsActivity;
import info.otrack.distributorapp.ui.adapters.OrderListAdapter;
import info.otrack.distributorapp.ui.fragments.base.LoadDataFragment;
import info.otrack.distributorapp.ui.presenters.JourneyDetailsPresenter;
import info.otrack.distributorapp.ui.presenters.base.IPresenter;

public class JourneyDetailsFragment extends LoadDataFragment<Journey> {

    private JourneyDetailsPresenter journeyPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View viewContainer = super.onCreateView(inflater, container, savedInstanceState);

        this.journeyPresenter = (JourneyDetailsPresenter) this.presenter;

        if(!getArguments().isEmpty() && getArguments().getInt(JourneyDetailsActivity.ID_TAG) != 0) {
            this.journeyPresenter.setId(getArguments().getInt(JourneyDetailsActivity.ID_TAG));
            this.journeyPresenter.execute();
        }

        return viewContainer;
    }

    @Override
    public void onResume() {
        super.onResume();

        this.showLoading();
        this.journeyPresenter.execute();
    }

    @Override
    protected IPresenter createPresenter() {
        return new JourneyDetailsPresenter(this);
    }

    /**
     * Updates the view
     * @param journey
     */
    @Override
    public void setData(Journey journey) {
        this.showResults();

        TextView id = this.mainView.findViewById(R.id.id_order);
        id.setText(journey.id + "");

        TextView date = this.mainView.findViewById(R.id.textView4);
        date.setText(Utils.getHoursFromDate(journey.createdAt));

        OrderListAdapter adapter = new OrderListAdapter(getContext(), journey.orders);
        ListView listView = this.mainView.findViewById(R.id.orders_list);


        listView.setOnItemClickListener((parent, view, position, id1) -> {
            Order item = (Order)listView.getAdapter().getItem(position);
            Intent intent = OrderDetailsActivity.createIntent(getContext(), item.id, journey);
            getContext().startActivity(intent);
        });

        listView.setAdapter(adapter);
    }

    @Override
    protected int getLayout() {
        return R.layout.journey_details;
    }
}
