package info.otrack.distributorapp.ui.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hypertrack.lib.HyperTrack;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import info.otrack.distributorapp.R;
import info.otrack.distributorapp.helpers.Utils;
import info.otrack.distributorapp.model.CurrentState;
import info.otrack.distributorapp.model.Order;
import info.otrack.distributorapp.ui.activities.OrderDetailsActivity;
import info.otrack.distributorapp.ui.adapters.OrderItemsListAdapter;
import info.otrack.distributorapp.ui.fragments.base.LoadDataFragment;
import info.otrack.distributorapp.ui.presenters.base.IPresenter;
import info.otrack.distributorapp.ui.presenters.OrderDetailsPresenter;

public class OrderDetailsFragment extends LoadDataFragment<Order> {

    public static int REQUEST_CALL = 985; // code to check for call permission
    private String callNumber = "";
    private OrderDetailsPresenter currentPresenter;
    private Order order;
    CircularProgressButton btn;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View viewContainer = super.onCreateView(inflater, container, savedInstanceState);

        // we need some specific methods of the presenter
        currentPresenter = (OrderDetailsPresenter) this.presenter;

        if (!getArguments().isEmpty() && getArguments().getInt(OrderDetailsActivity.ID_TAG) != 0) {
            currentPresenter.setId(getArguments().getInt(OrderDetailsActivity.ID_TAG));
            currentPresenter.execute();
        }

        return viewContainer;
    }

    /**
     * Updates the view
     */
    @Override
    public void setData(Order order) {
        this.showResults();
        this.order = order;

        TextView address = this.mainView.findViewById(R.id.order_address1);
        address.setText(order.address);

        TextView address2 = this.mainView.findViewById(R.id.order_address21);
        address2.setText(order.address2);

        TextView postal = this.mainView.findViewById(R.id.order_postal1);
        postal.setText(order.postalCode);

        TextView date = this.mainView.findViewById(R.id.order_created1);
        date.setText(Utils.getHoursFromDate(order.createdAt));

        TextView delivered = this.mainView.findViewById(R.id.order_delivered1);
        TextView delivered_text = this.mainView.findViewById(R.id.order_delivered);
        delivered.setVisibility(View.INVISIBLE);
        delivered_text.setVisibility(View.INVISIBLE);
        delivered.setText(Utils.getHoursFromDate(order.deliveredAt));

        OrderItemsListAdapter adapter = new OrderItemsListAdapter(getContext(), order.items);
        ListView listView = this.mainView.findViewById(R.id.order_items);

        btn = this.mainView.findViewById(R.id.start_button);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btn.setBackgroundTintList(getActivity().getResources().getColorStateList(R.color.colorPrimary));
        }

        btn.setTextColor(Color.parseColor("#FFFFFF"));

        // call customer
        this.mainView.findViewById(R.id.call_customer).setOnClickListener(v -> {
            callNumber = "tel:" + order.customer.phoneNumber;

            onCall(); // ask permissions and call
        });

        // launch google maps directions
        this.mainView.findViewById(R.id.openmaps).setOnClickListener(v -> {
            launchGoogleMaps(order);
        });

        listView.setAdapter(adapter);

        if(order.deliveredAt != null || CurrentState.isCompleted(getViewContext(), order.id)) {
            deliveryEnded();
            delivered.setVisibility(View.VISIBLE);
            delivered_text.setVisibility(View.VISIBLE);
            return;
        }

        try {
            int currentOrderId = CurrentState.getDelivery(getViewContext());

            if(currentOrderId != order.id) {
                btn.setEnabled(false);
                btn.setText(R.string.delivery_in_progress);
                return;
            }

            // this order is in progress
            this.deliveryStarted();

        } catch (Exception ignored) {
            // no current delivery

            if(order.onGoing) {
                this.deliveryStarted();
                CurrentState.setDelivery(getViewContext(), order.id);
            }
        }

        // start/stop tracking
        btn.setOnClickListener(v -> {
            // check permissions and start/stop tracking
            currentPresenter.toggleDelivery(this.order);
        });
    }

    private void launchGoogleMaps(Order order) {
        try {
            String finaladdr = order.address + ", " + order.postalCode;
            String encoded = URLEncoder.encode(finaladdr, "UTF-8");
            String uri = "https://www.google.com/maps/dir/?api=1&travelmode=driving&destination="+encoded;

            Intent intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse(uri));
            startActivity(intent);

        } catch (UnsupportedEncodingException e) {
            Toast.makeText(getContext(), "Address is not valid",
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Begin the delivery process
     */
    @UiThread
    public void beginDelivery() {
        btn.startAnimation();
    }

    /**
     * Delivery started
     */
    @UiThread
    public void deliveryStarted() {
        btn.revertAnimation(() -> {
            btn.setText(R.string.finish_delivery);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btn.setBackgroundTintList(getActivity().getResources().getColorStateList(R.color.colorError));
            }
        });

        btn.setText(R.string.finish_delivery);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btn.setBackgroundTintList(getActivity().getResources().getColorStateList(R.color.colorError));
        }
    }

    /**
     * Delivery is being marked as done
     */
    @UiThread
    public void beginEndDelivery() {
        btn.startAnimation();
    }

    /**
     * Delivery is done
     */
    @UiThread
    public void deliveryEnded() {
        btn.revertAnimation(() -> {
            btn.setEnabled(false);
            btn.setText(R.string.delivery_completed);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btn.setBackgroundTintList(getActivity().getResources().getColorStateList(R.color.colorSuccess));
            }
        });

        btn.setEnabled(false);
        btn.setText(R.string.delivery_completed);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btn.setBackgroundTintList(getActivity().getResources().getColorStateList(R.color.colorSuccess));
        }
    }

    /**
     * Something odd happened
     */
    @UiThread
    public void deliveryFailed() {
        btn.revertAnimation(() -> {
            btn.setText(R.string.start_delivery);
            btn.setEnabled(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btn.setBackgroundTintList(getActivity().getResources().getColorStateList(R.color.colorPrimary));
            }
        });

        btn.setText(R.string.start_delivery);
        btn.setEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btn.setBackgroundTintList(getActivity().getResources().getColorStateList(R.color.colorPrimary));
        }
    }

    /**
     * Request permissions to call
     */
    public void onCall() {
        int permissionCheck = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{Manifest.permission.CALL_PHONE},
                    REQUEST_CALL);
        } else {
            startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse(this.callNumber)));
        }
    }


    @Override
    protected int getLayout() {
        return R.layout.order_details;
    }

    @Override
    protected IPresenter createPresenter() {
        return new OrderDetailsPresenter(this);
    }

    @Override
    public void onDestroy() {
        if(btn != null) {
            btn.dispose();
        }

        super.onDestroy();
    }
}
