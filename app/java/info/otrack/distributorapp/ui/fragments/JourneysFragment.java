package info.otrack.distributorapp.ui.fragments;

import info.otrack.distributorapp.ui.fragments.common.JourneyListableFragment;
import info.otrack.distributorapp.ui.presenters.JourneyListPresenter;
import info.otrack.distributorapp.ui.presenters.base.IPresenter;


public class JourneysFragment extends JourneyListableFragment {

    @Override
    protected IPresenter createPresenter() {
        return new JourneyListPresenter(this);
    }
}
