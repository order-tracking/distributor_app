package info.otrack.distributorapp.services;

import com.hypertrack.lib.HyperTrackFirebaseMessagingService;

/**
 * Required for Hypertrack FCM communication
 */
public class HypertrackFCMService extends HyperTrackFirebaseMessagingService {
}


