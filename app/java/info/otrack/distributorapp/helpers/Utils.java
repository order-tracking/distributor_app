package info.otrack.distributorapp.helpers;

import android.content.Context;
import android.net.ConnectivityManager;

import com.auth0.android.jwt.JWT;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import info.otrack.distributorapp.model.Employee;

public class Utils {

    public static Employee getEmployeeFromAccessToken(String accessToken) {
        JWT jwt = new JWT(accessToken);

        Employee user = new Employee();
        user.companyId = jwt.getClaim("companyId").asInt();
        user.id = jwt.getClaim("employeeId").asInt();
        user.name = jwt.getClaim("name").asString();
        user.phoneNumber = jwt.getClaim("phoneNumber").asString();
        user.companyName = jwt.getClaim("companyName").asString();
        user.role = jwt.getClaim("role").asString();

        return user;
    }

    public static String getHoursFromDate(Date date) {
        if(date == null) return "";

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int hours = cal.get(Calendar.HOUR_OF_DAY);
        int minutes = cal.get(Calendar.MINUTE);

       return padLeft(hours + "", 2, "0") + ":" + padLeft(minutes + "", 2, "0");
    }

    public static String padLeft(String str, int length, String padChar) {
        String pad = "";
        for (int i = 0; i < length; i++) {
            pad += padChar;
        }
        return pad.substring(str.length()) + str;
    }

    public static double doubleRandom(double min, double max) {
        Random r = new Random();
        return (r.nextInt((int)((max-min)*10+1))+min*10) / 10.0;
    }

    public static boolean isNetworkConnected(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
